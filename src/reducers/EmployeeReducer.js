import { 
    EMPLOYEE_DATA_UPDATE,
    EMPLOYEE_SAVE,
    EMPLOYEE_EDIT
} from '../actions/types';

const INITIAL_STATE = { 
    name: '', 
    phone: '',
    shift: 'Monday',
};

//KEY INTERPOLATION: Ex : name: jane
//SAME:
//const newState = {};
//newState[action.payload.prop] = action.payload.value;
//return { ...state, ...newState };   
 //return { ...state, [action.payload.prop]: action.payload.value };  
export default (state = INITIAL_STATE, action) => {
    //console.log(action);
    switch (action.type) {
        case EMPLOYEE_DATA_UPDATE:
            return { ...state, [action.payload.props]: action.payload.value };  
        case EMPLOYEE_SAVE:
            return INITIAL_STATE; 
        case EMPLOYEE_EDIT:
            console.log(action);
            return INITIAL_STATE; 
        default:        
            return state;
    }
};
