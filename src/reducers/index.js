import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeReducer from './EmployeeReducer';
import EmployeeList from './EmployeeListReducer';

export default combineReducers({
    auth: AuthReducer,
    employee: EmployeeReducer, 
    employees: EmployeeList   
});
