import React, { Component } from 'react';
//import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
//import { Header } from './components/common';
//import LoginForm from './components/LoginForm';
import Router from './Router';


class App extends Component {

    componentWillMount() {
        // Initialize Firebase
        const config = {
          apiKey: 'AIzaSyCkfppV7vBYmig_IEVz8qFyRzLdhsmLYjM',
          authDomain: 'manager-57fdf.firebaseapp.com',
          databaseURL: 'https://manager-57fdf.firebaseio.com',
          projectId: 'manager-57fdf',
          storageBucket: '',
          messagingSenderId: '675570798982'
        };
        firebase.initializeApp(config);
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
