import _ from 'lodash';
import Communications from 'react-native-communications';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    employeeEdit, 
    employeeDataUpdate,
    employeeDelete
} from '../actions';
import { Button, Card, CardSection, Confirm } from './common';
import EmployeeForm from './EmployeeForm';

class EmployeeEdit extends Component {
    state = { showModal: false }    


    componentWillMount() {
        _.each(this.props.employee, (value, props) => {
            console.log({ props, value });
            this.props.employeeDataUpdate({ props, value });
        });
        // console.log({ prop, value });
        // this.props.employeeDataUpdate({ name: 'John', phone: '33', shift: 'Tuesday' });
    }

    employeeEdit() {
        const { name, phone, shift } = this.props;
        this.props.employeeEdit({ name, phone, shift, uid: this.props.employee.uid });
    }

    onFirePress() {
        this.setState({ showModal: !this.state.showModal });        
    }
    
    onDecline() {
        this.setState({ showModal: false });        
    }

    onAccept() {
        this.props.employeeDelete({ uid: this.props.employee.uid });
        //this.setState({ showModal: false });  
    }

    employeeText() {
        const { name, phone, shift } = this.props;
        Communications.text(phone, `${name}, your upcoming shift is on ${shift}`);
    }

    render() {
        return (
            <Card >
                <EmployeeForm />
                <CardSection>                  
                    <Button onPress={this.employeeEdit.bind(this)}> 
                        Save 
                    </Button>
                </CardSection>
                <CardSection>                  
                    <Button onPress={this.employeeText.bind(this)}> 
                        Text
                    </Button>
                </CardSection>
                <CardSection>
                    <Button onPress={this.onFirePress.bind(this)}> 
                        Fire 
                    </Button>
                </CardSection>
                <Confirm
                    visible={this.state.showModal}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDecline.bind(this)}
                >
                    Are you sure you want to fire this?
                </Confirm>   
            </Card>        
                    
        );
    }
}

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employee;

    return { name, phone, shift };
};

export default connect(mapStateToProps, {
    employeeEdit,
    employeeDataUpdate,
    employeeDelete
})(EmployeeEdit);
