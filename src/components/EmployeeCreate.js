import React, { Component } from 'react';
import { connect } from 'react-redux';
import { employeeSave } from '../actions';
import { Button, Card, CardSection } from './common';
import EmployeeForm from './EmployeeForm';

class EmployeeCreate extends Component {
    employeeSave() {
        const { name, phone, shift } = this.props;
        this.props.employeeSave({ name, phone, shift });
    }


    render() {
        return (
            <Card>
                <EmployeeForm {...this.props} />                    
                <CardSection>                   
                    <Button onPress={this.employeeSave.bind(this)}> 
                        Create 
                    </Button>
                </CardSection>
            </Card>                     
        );
    }
}

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employee;

    return { name, phone, shift };
};

export default connect(mapStateToProps, {
    employeeSave
})(EmployeeCreate);
