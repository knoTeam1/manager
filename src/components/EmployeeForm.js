import React, { Component } from 'react';
import { Picker, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { 
    employeeDataUpdate
} from '../actions';
import { Input, CardSection } from './common';

class EmployeeForm extends Component {
    render() {
        return (
            <View>
                <CardSection>
                    <Input
                        label="Name"
                        onChangeText={value => 
                            this.props.employeeDataUpdate({ props: 'name', value })}
                        value={this.props.name}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        label="Phone"
                        onChangeText={text => 
                            this.props.employeeDataUpdate({ props: 'phone', value: text })
                        }
                        value={this.props.phone}
                    />
                </CardSection>
                <CardSection >
                    <Text style={styles.pickerTextStyle}>Shift</Text>
                    <Picker
                        style={{ flex: 1 }}
                        selectedValue={this.props.shift}
                        onValueChange={value => 
                            this.props.employeeDataUpdate({ props: 'shift', value })}
                    >
                        <Picker.Item label="Monday" value="Monday" />
                        <Picker.Item label="Tuesday" value="Tuesday" />
                        <Picker.Item label="Wednesday" value="Wednesday" />
                        <Picker.Item label="Thursday" value="Thursday" />
                        <Picker.Item label="Friday" value="Friday" />
                        <Picker.Item label="Saturday" value="Saturday" />
                        <Picker.Item label="Sunday" value="Sunday" />
                    </Picker>                    
                </CardSection>       
            </View>         
        );
    }
}

const styles = {
    pickerTextStyle: {
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 40
    }
};

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employee;

    return { name, phone, shift };
};

export default connect(mapStateToProps, { employeeDataUpdate })(EmployeeForm);
