import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import { 
    EMPLOYEE_DATA_UPDATE,
    EMPLOYEE_SAVE,
    EMPLOYEES_FETCH_SUCCESS,
    EMPLOYEE_EDIT
} from './types';


///// EMPLOYEE FORM
export const employeeDataUpdate = ({ props, value }) => {
    return {
        type: EMPLOYEE_DATA_UPDATE,
        payload: { props, value }
    };
};

export const employeeSave = ({ name, phone, shift }) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
            .push({ name, phone, shift })
            .then(() => {
                dispatch({ type: EMPLOYEE_SAVE });
                Actions.pop({ type: 'reset ' });
            });
    };
};

export const employeesFetch = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
            .on('value', snapshot => {
                dispatch({ type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val() });    
            });
    };
};

export const employeeEdit = ({ name, phone, shift, uid }) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
            .set({ name, phone, shift })
            .then(() => {
                dispatch({ type: EMPLOYEE_EDIT });
                Actions.pop({ type: 'reset ' });
            });
    };
};

export const employeeDelete = ({ uid }) => {
    const { currentUser } = firebase.auth();

    return () => {
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
            .remove()    
            .then(() => {
                Actions.pop({ type: 'reset ' });
            });
    };
};
